import { useState } from 'react';
import './App.css';

function App() {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [status, setStatus] = useState("");

  const handleSubmit = async (e: any) => {
    setStatus("Отправка запроса");
    e.preventDefault();
    await fetch('/post', { method: 'post', body: JSON.stringify({ username, password }) })
      .then((data: any) => {
        console.log(data);
        if (data.ok) {
          setStatus("Запрос успешно выполнен");
        }
        else {
          console.log(data);
          if (data.statusText) {
            setStatus("Запрос выполнился с ошибкой: " + data.statusText);
          }
          else {
            setStatus("Json API вернул success != true" + JSON.stringify(data));
          }
          return data;
        }
      });
  };

  return (
    <div className="App">
      <form onSubmit={handleSubmit} className="App-header">
        <label>Логин или Email</label>
        <input
          name="login"
          autoComplete="false"
          className="form-control"
          type="text"
          value={username}
          onChange={(e) => setUserName(e.target.value)}
        />
        <label>Пароль</label>
        <input
          name="password"
          className="form-control"
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <button type='submit'>Войти</button>
        {status && <div>{status}</div>}
      </form>
    </div >
  );
}

export default App;
